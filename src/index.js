import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import createStore from './store/create-store'
import { Provider } from 'react-redux'
import 'semantic-ui-css/semantic.min.css'
import './index.scss'

const store = createStore()

const Entry = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  )
}

ReactDOM.render(<Entry />, document.getElementById('root'))
