import * as _ from 'lodash'
import { call, put, takeLatest } from 'redux-saga/effects'
import api from '../services/api'
import { OrderActions, OrderActionTypes } from '../actions/OrderActions'

/**
 * Saga handler for getting order book data
 */
const getOrderBook = function * (action) {
  const response = yield call(api.getOrderBook)

  if (!response.ok) {
    const error = _.get(response, 'error')
    yield put(OrderActions.getOrderBookFail(error))
    return
  }

  yield put(OrderActions.getOrderBookSuccess(_.get(response, 'data')))
}

const sagas = []

sagas.push(takeLatest(OrderActionTypes.GET_ORDER_BOOK_REQUEST, getOrderBook))

export default sagas
