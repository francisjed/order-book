import * as _ from 'lodash'
import { call, put, takeLatest } from 'redux-saga/effects'
import api from '../services/api'
import { UserActions, UserActionTypes } from '../actions/UserActions'
import { WalletActions } from '../actions/WalletActions'

/**
 * Saga handler for getting user information
 */
const getUserInfo = function * (action) {
  const response = yield call(api.getUserInfo)

  if (!response.ok) {
    const error = _.get(response, 'error')
    yield put(UserActions.getUserInfoFail(error))
    return
  }

  const user = _.get(response, 'data')
  yield put(WalletActions.setBalances(user.balances))
  yield put(UserActions.getUserInfoSuccess(user))
}

const sagas = []

sagas.push(takeLatest(UserActionTypes.GET_USER_INFO_REQUEST, getUserInfo))

export default sagas
