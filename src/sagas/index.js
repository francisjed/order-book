import { all } from 'redux-saga/effects'
import user from './user'
import order from './order'

const sagas = [].concat(user, order)

export default function * root () {
  yield all(sagas)
}
