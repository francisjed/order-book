import { combineReducers } from 'redux'
import user from './reducers/user'
import order from './reducers/order'
import wallet from './reducers/wallet'

export const makeRootReducer = asyncReducers => {
  return combineReducers({
    // Insert new reducers here
    order,
    user,
    wallet,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
