import _ from 'lodash'
import { createReducer } from 'reduxsauce'
import { OrderActionTypes } from '../../actions/OrderActions'

/**
 * Add order to order book list and open orders list
 * @param {*} order
 * @param {*} state
 */
const addOrderToList = (order, state) => {
  let { asks, bids } = state.orderBook
  state.openOrders.push(order)

  let list = order.type === 'bid' ? bids : asks
  list.push(order)
  list = _.sortBy(list, i => -i.price)

  if (order.type === 'bid') {
    state.orderBook.bids = list
  } else {
    state.orderBook.asks = list
  }

  return { ...state }
}

/**
 * Remove filled orders from the list
 * @param {*} order
 * @param {*} state
 */
const removeOrderFromList = (order, state) => {
  let { asks, bids } = state.orderBook

  let list = order.type === 'bid' ? bids : asks
  _.remove(list, i => i.id === order.id)

  if (order.type === 'bid') {
    state.orderBook.bids = list
  } else {
    state.orderBook.asks = list
  }

  if (order.owned) {
    _.remove(state.openOrders, i => i.id === order.id)
    state.closedOrders.push(order)
  }

  return { ...state }
}

/**
 * Calculate adjustments of different wallet
 * @param {*} order
 * @param {*} volume
 * @param {*} state
 */
const calculateWalletAdjustments = (order, volume, state) => {
  if (order.type === 'bid') {
    state.adjustments.php = volume * order.price
  } else {
    state.adjustments.tcn = volume
  }

  return state
}

/**
 * Process the placed limit order
 * @param {*} order
 * @param {*} state
 */
const processLimitOrder = (order, state) => {
  const { orderBook } = state
  let { asks, bids } = orderBook
  asks = asks.slice()
  bids = bids.slice()
  let matchedIndex = null
  let matchedOrder = null

  if (order.type === 'bid') {
    matchedIndex = _.findIndex(asks, i => i.price <= order.price)
    matchedOrder = asks[matchedIndex]
  } else {
    bids = bids.slice().reverse()
    matchedIndex = _.findIndex(bids, i => i.price >= order.price)
    matchedOrder = bids[matchedIndex]
  }

  if (!matchedOrder) {
    state = addOrderToList(order, state)
    state = calculateWalletAdjustments(order, order.volume, state)
    return { ...state }
  }

  // This is all or nothing
  // If there is no enough total volume to buy or sell for a specific price, it will be added to the order book list
  if (matchedOrder.volume < order.volume) {
    state = addOrderToList(order, state)
    state = calculateWalletAdjustments(order, order.volume, state)
    return { ...state }
  }

  order.price = matchedOrder.price
  state = calculateWalletAdjustments(order, order.volume, state)
  state = calculateWalletAdjustments(matchedOrder, order.volume, state)
  state.closedOrders.push(order)

  if (matchedOrder.volume === order.volume) {
    state = removeOrderFromList(matchedOrder, state)
    return { ...state }
  }

  // Update the match order in the order book
  matchedOrder.volume -= order.volume

  console.log(matchedOrder)
  if (order.type === 'bid') {
    asks[matchedIndex] = matchedOrder
    state.orderBook = {
      ...orderBook,
      asks
    }
  } else {
    bids[matchedIndex] = matchedOrder
    state.orderBook = {
      ...orderBook,
      bids
    }
  }

  return { ...state }
}

/**
 * Calculate the order book spread
 * @param {array} bids
 * @param {array} asks
 */
const calculateSpread = (bids, asks) => {
  const lowestAsk = _.last(asks)
  const highestBid = _.first(bids)

  return (lowestAsk.price - highestBid.price).toFixed(2)
}

const initialState = {
  orderBook: {
    asks: [],
    bids: [],
    spread: 0
  },
  orderCount: 0,
  openOrders: [],
  closedOrders: [],
  loading: false,
  error: '',
  adjustments: {
    php: 0,
    tcn: 0
  }
}

const actionHandlers = {}

actionHandlers[OrderActionTypes.GET_ORDER_BOOK_REQUEST] = (state) => {
  state.loading = true
  state.error = ''

  return { ...state }
}

actionHandlers[OrderActionTypes.GET_ORDER_BOOK_SUCCESS] = (state, action) => {
  const { orderBook } = action

  let bids = []
  let asks = []

  for (let order of orderBook) {
    if (order.type === 'bid') {
      bids.push(order)
    } else {
      asks.push(order)
    }
  }

  bids = _.sortBy(bids, i => -i.price)
  asks = _.sortBy(asks, i => -i.price)

  state.orderBook.spread = calculateSpread(bids, asks)
  state.orderBook.asks = asks
  state.orderBook.bids = bids
  state.orderCount = orderBook.length

  state.loading = false

  return { ...state }
}

actionHandlers[OrderActionTypes.GET_ORDER_BOOK_FAIL] = (state, action) => {
  state.loading = false
  state.error = action.error.message

  return { ...state }
}

actionHandlers[OrderActionTypes.PLACE_ORDER] = (state, action) => {
  const { orderType, order } = action
  order.id = `order-book-${state.orderCount}`
  order.owned = true
  state.adjustments = {
    php: 0,
    tcn: 0
  }

  switch (orderType) {
    case 'limit':
      state = processLimitOrder(order, state)
      break
  }

  const { bids, asks } = state.orderBook
  state.orderCount++
  state.orderBook.spread = calculateSpread(bids, asks)
  return { ...state }
}

export default createReducer(initialState, actionHandlers)
