import _ from 'lodash'
import { createReducer } from 'reduxsauce'
import { WalletActionTypes } from '../../actions/WalletActions'

const initialState = {
  balances: []
}

const actionHandlers = {}

actionHandlers[WalletActionTypes.SET_BALANCES] = (state, action) => {
  const { balances } = action
  return { balances }
}

actionHandlers[WalletActionTypes.APPLY_ADJUSTMENTS] = (state, action) => {
  const { adjustments } = action

  for (let adjustment of adjustments) {
    const index = _.findIndex(state.balances, i => i.symbol === adjustment.symbol)

    state.balances[index].balance += adjustment.amount
  }

  return { ...state }
}

export default createReducer(initialState, actionHandlers)
