import { createReducer } from 'reduxsauce'
import { UserActionTypes } from '../../actions/UserActions'

const initialState = {
  name: '',
  loading: false,
  error: ''
}

const actionHandlers = {}

actionHandlers[UserActionTypes.GET_USER_INFO_REQUEST] = (state) => {
  state.loading = true
  state.error = ''

  return { ...state }
}

actionHandlers[UserActionTypes.GET_USER_INFO_SUCCESS] = (state, action) => {
  const { user } = action
  state.loading = false
  state.name = user.name
  return { ...state }
}

actionHandlers[UserActionTypes.GET_USER_INFO_FAIL] = (state, action) => {
  state.loading = false
  state.error = action.error.message

  return { ...state }
}

export default createReducer(initialState, actionHandlers)
