import { applyMiddleware, createStore as createReduxStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import makeRootReducer from './reducers'
import createSagaMiddleware from 'redux-saga'
import sagas from '../sagas'

const sagaMiddleware = createSagaMiddleware()

const createStore = (initialState = {}) => {
  // Middleware Configuration
  const middleware = [ thunk, sagaMiddleware ]

  // Store Enhancers
  const enhancers = []
  let composeEnhancers = composeWithDevTools

  // Store Instantiation and HMR Setup
  const store = createReduxStore(makeRootReducer(), initialState, composeEnhancers(applyMiddleware(...middleware), ...enhancers))
  let sagasManager = sagaMiddleware.run(sagas)

  store.asyncReducers = {}

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const reducers = require('./reducers').default
      store.replaceReducer(reducers(store.asyncReducers))

      const newYieldedSagas = require('@sagas').default
      sagasManager.cancel()
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware.run(newYieldedSagas)
      })
    })
  }

  return store
}

export default createStore
