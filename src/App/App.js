import React from 'react'
import { Grid, Button, Modal } from 'semantic-ui-react'
import UserInfo from '../components/UserInfo'
import OrderBook from '../components/OrderBook'
import OrderForm from '../components/OrderForm'
import OrderList from '../components/OrderList'
import Panel from '../components/Panel'
import './App.scss'

/**
 * The Main App component - this is the entry point of the application
 */
class App extends React.Component {
  state = {
    showOrderForm: false
  }

  componentDidMount () {
    this.props.getUserInfo()
    this.props.getOrderBook()
  }

  /**
   * Handles clicking the order button event
   */
  handleOrderClick = () => {
    this.setState({
      showOrderForm: true
    })
  }

  /**
   * Handles close order form modal event
   */
  handleOrderFormClose = () => {
    this.setState({
      showOrderForm: false
    })
  }

  handleOrderFormSubmit = (order) => {
    this.props.placeLimitOrder(order)
    this.setState({ showOrderForm: false })
  }

  render () {
    const { showOrderForm } = this.state
    const { user, orderBook, currencies, openOrders, closedOrders, balances } = this.props
    const { asks, bids, spread } = orderBook
    const { tnc, php } = currencies
    const tncBalance = tnc ? tnc.balance : 0
    const phpBalance = php ? php.balance : 0

    return (
      <div className='main-page'>
        <Modal dimmer='blurring' onClose={this.handleOrderFormClose} size='mini' open={showOrderForm}>
          <Modal.Header>Limit Order</Modal.Header>
          <Modal.Content>
            <OrderForm tncBalance={tncBalance} phpBalance={phpBalance} onSubmit={this.handleOrderFormSubmit} />
          </Modal.Content>
        </Modal>
        <Grid container columns={2}>
          <Grid.Column largeScreen={5}>
            <UserInfo fullname={user.name} balances={balances} />
            <Button onClick={this.handleOrderClick} color='green' fluid>LIMIT ORDER</Button>
          </Grid.Column>
          <Grid.Column largeScreen={11} className='panels'>
            <Panel header='Order Book'>
              <OrderBook asks={asks} bids={bids} spread={spread} />
            </Panel>
            <Panel header='Open Orders'>
              <OrderList orders={openOrders} />
            </Panel>
            <Panel header='Closed Orders'>
              <OrderList orders={closedOrders} />
            </Panel>
          </Grid.Column>
        </Grid>
      </div>
    )
  }
}

export default App
