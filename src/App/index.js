import App from './App'
import _ from 'lodash'
import { connect } from 'react-redux'
import { UserActions } from '../actions/UserActions'
import { OrderActions } from '../actions/OrderActions'

// Map action creators for component to consume
const mapActionCreators = (dispatch, getState) => {
  return {
    getUserInfo: () => {
      dispatch(UserActions.getUserInfoRequest())
    },
    getOrderBook: () => {
      dispatch(OrderActions.getOrderBookRequest())
    },
    placeLimitOrder: (order) => {
      dispatch(OrderActions.placeOrder('limit', order))
    }
  }
}

// Map state to props for component to consume
const mapStateToProps = (state) => {
  const { user, order, wallet } = state
  const { balances = [] } = wallet

  const tnc = _.find(balances, i => i.symbol === 'TestCoin')
  const php = _.find(balances, i => i.symbol === 'PHP')

  return {
    balances,
    closedOrders: order.closedOrders,
    currencies: {
      tnc,
      php
    },
    openOrders: order.openOrders,
    orderBook: order.orderBook,
    user
  }
}

export default connect(mapStateToProps, mapActionCreators)(App)
