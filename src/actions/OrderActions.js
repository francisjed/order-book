import { createActions } from 'reduxsauce'
import { WalletActions } from './WalletActions'

const { Types, Creators } = createActions({
  getOrderBookRequest: null,
  getOrderBookSuccess: ['orderBook'],
  getOrderBookFail: ['error'],
  placeOrder: (orderType, order) => {
    return async (dispatch, getState) => {
      await dispatch({ type: 'PLACE_ORDER', orderType, order })
      const state = getState()
      const { adjustments } = state.order
      dispatch(WalletActions.applyAdjustments([
        {
          symbol: 'PHP',
          amount: adjustments.php * (order.type === 'bid' ? -1 : 1)
        },
        {
          symbol: 'TestCoin',
          amount: adjustments.tcn * (order.type === 'bid' ? 1 : -1)
        }
      ]))
    }
  }
})

export const OrderActions = Creators
export const OrderActionTypes = Types
