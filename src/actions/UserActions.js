import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  getUserInfoRequest: null,
  getUserInfoSuccess: ['user'],
  getUserInfoFail: ['error'],
  clearUserList: null
})

export const UserActions = Creators
export const UserActionTypes = Types
