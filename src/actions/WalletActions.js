import { createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  setBalances: ['balances'],
  applyAdjustments: ['adjustments']
})

export const WalletActions = Creators
export const WalletActionTypes = Types
