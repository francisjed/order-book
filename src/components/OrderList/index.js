import React from 'react'
import { Table, Message } from 'semantic-ui-react'
import './OrderList.scss'

/**
 * Component for listing submitted orders of the user
 */
const OrderList = (props) => {
  const { orders = [] } = props

  if (orders.length === 0) {
    return (
      <Message
        info
        content='There are no orders for this list.'
      />
    )
  }

  return (
    <Table basic inverted className='order-list' textAlign='right' celled={false}>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Price</Table.HeaderCell>
          <Table.HeaderCell>Volume</Table.HeaderCell>
          <Table.HeaderCell>Type</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {orders.map((order, i) => {
          return (
            <Table.Row key={i} className={order.type}>
              <Table.Cell>{order.price}</Table.Cell>
              <Table.Cell>{order.volume}</Table.Cell>
              <Table.Cell className='type'>{order.type}</Table.Cell>
            </Table.Row>
          )
        })}
      </Table.Body>
    </Table>
  )
}

export default OrderList
