import React from 'react'
import PropTypes from 'prop-types'
import formatToCurrency from '../../utils/formatToCurrency'
import { Segment } from 'semantic-ui-react'
import './UserInfo.scss'

/**
 * Component for displaying user information and balanaces
 */
const UserInfo = (props) => {
  const { fullname, balances = [] } = props
  const names = fullname.split(' ')
  let initials = ''

  if (names.length > 1) {
    initials = names[0].charAt(0) + names[names.length - 1].charAt(0)
  } else {
    initials = names[0] || ''
  }

  return (
    <div className='user-info'>
      {fullname && (
        <div className='user'>
          <div className='initials'>
            {initials}
          </div>
          <span className='fullname'>
            {fullname}
          </span>
        </div>
      )}
      {balances.map((item, i) => {
        const balance = item.symbol === 'PHP' ? item.balance.toFixed(2) : item.balance
        return (
          <Segment className='balance' key={i} textAlign='right' clearing>
            <div className='label'>{item.symbol}</div>
            <div className='value'>{formatToCurrency(balance)}</div>
          </Segment>
        )
      })}
    </div>
  )
}

UserInfo.propTypes = {
  fullname: PropTypes.string,
  balances: PropTypes.arrayOf(PropTypes.shape({
    symbol: PropTypes.string.isRequired,
    balance: PropTypes.number.isRequired
  }))
}

export default UserInfo
