import React from 'react'
import { Segment, Header } from 'semantic-ui-react'
import './Panel.scss'

/**
 * Panel component
 */
export default ({ children, header }) => {
  return (
    <Segment className='panel'>
      {header && <Header as='h4' className='panel-header'>{header}</Header>}
      {children}
    </Segment>
  )
}
