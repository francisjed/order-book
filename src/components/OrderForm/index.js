import React from 'react'
import PropTypes from 'prop-types'
import { Form, Message } from 'semantic-ui-react'

const types = [
  {
    key: 0,
    text: 'Buy',
    value: 'bid'
  },
  {
    key: 1,
    text: 'Sell',
    value: 'ask'
  }
]

/**
 * Order form component
 */
class OrderForm extends React.Component {
  state = {
    type: '',
    price: '',
    volume: '',
    total: '',
    error: ''
  }

  /**
   * Handles input change
   */
  handleChange = (e, { name, value }) => {
    if (name === 'total') {
      const { price } = this.state
      const volume = price ? (value / price).toFixed(4) : 0

      this.setState({
        volume,
        [name]: value
      })
      return
    }

    const { price = 0, volume = 0 } = this.state
    let total = 0

    switch (name) {
      case 'price':
        total = (volume * value).toFixed(2)
        break
      case 'volume':
        total = (price * value).toFixed(2)
        break
      default:
        total = (price * volume).toFixed(2)
        break
    }

    this.setState({ [name]: value, total })
  }

  /**
   * Handles form submission
   */
  handleSubmit = (e) => {
    e.preventDefault()

    this.setState({ error: '' })

    const { onSubmit, phpBalance, tncBalance } = this.props
    const { type, price = 0, volume = 0, total = 0 } = this.state

    if (!type) {
      this.setState({ error: 'Type field is required.' })
      return
    }

    if (!price || !volume || parseFloat(price) === 0 || parseFloat(volume) === 0) {
      this.setState({ error: 'Price or Volume field should be greater than zero.' })
      return
    }

    switch (type) {
      case 'bid':
        if (total > phpBalance) {
          this.setState({ error: 'You do not have enough balance in your PHP wallet.' })
          return
        }
        break
      case 'ask':
        if (total > tncBalance) {
          this.setState({ error: 'You do not have enough balance in your TestCoin wallet.' })
          return
        }
        break
    }

    if (onSubmit) {
      const params = { type, price: parseFloat(price), volume: parseFloat(volume) }

      onSubmit(params)
    }
  }

  render () {
    const { type, price, volume, total, error } = this.state

    return (
      <Form error={error !== ''} onSubmit={this.handleSubmit}>
        <Form.Select required onChange={this.handleChange} name='type' label='Type' options={types} value={type} />
        <Form.Input required type='number' onChange={this.handleChange} name='price' label='Price' value={price} />
        <Form.Input required type='number' onChange={this.handleChange} name='volume' label='Volume' value={volume} />
        <Form.Input type='number' onChange={this.handleChange} name='total' label='Total' value={total} />
        <Message
          error
          content={error}
        />
        <Form.Button type='submit' onClick={this.handleSubmit} fluid positive>Submit</Form.Button>
      </Form>
    )
  }
}

OrderForm.propTypes = {
  onSubmit: PropTypes.func,
  phpBalance: PropTypes.number,
  tncBalance: PropTypes.number
}

export default OrderForm
