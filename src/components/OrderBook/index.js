import React from 'react'
import { Table } from 'semantic-ui-react'
import './OrderBook.scss'

const OrderBook = (props) => {
  const { asks = [], bids = [], spread = 0 } = props
  return (
    <Table basic inverted className='order-book' textAlign='right' celled={false}>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Total</Table.HeaderCell>
          <Table.HeaderCell>Price</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {asks.map((ask, i) => {
          return (
            <Table.Row key={i} className='ask'>
              <Table.Cell>{ask.volume}</Table.Cell>
              <Table.Cell className='price'>{ask.price}</Table.Cell>
            </Table.Row>
          )
        })}
        <Table.Row className='spread' textAlign='right'>
          <Table.Cell><strong>Spread</strong></Table.Cell>
          <Table.Cell><strong>{spread}</strong></Table.Cell>
        </Table.Row>
        {bids.map((bid, i) => {
          return (
            <Table.Row key={i} className='bid'>
              <Table.Cell>{bid.volume}</Table.Cell>
              <Table.Cell className='price'>{bid.price}</Table.Cell>
            </Table.Row>
          )
        })}
      </Table.Body>
      <Table.Footer>
        <Table.Row>
          <Table.HeaderCell>Total</Table.HeaderCell>
          <Table.HeaderCell>Price</Table.HeaderCell>
        </Table.Row>
      </Table.Footer>
    </Table>
  )
}

export default OrderBook
