import userData from '../../data/user.json'
import orderBookData from '../../data/order-book.json'

/**
 * This is a mock api service, simulating external requests
 */
const api = {}

api.getUserInfo = () => {
  // Just mocking the apisauce response
  return Promise.resolve({
    ok: true,
    data: userData,
    error: null
  })
}

api.getOrderBook = () => {
  // Just mocking the apisauce response
  return Promise.resolve({
    ok: true,
    data: orderBookData,
    error: null
  })
}

export default api
